attribute vec4 v_coord;
uniform mat4 m, v, p;
uniform vec3 shape_color;
varying vec3 f_color;

void main()
{
  mat4 mvp = p*v*m;
  gl_Position = mvp * v_coord;
  f_color = shape_color;
}
