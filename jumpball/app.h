#ifndef APP_H
#define APP_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdexcept>
/* Use glew.h instead of gl.h to get all the GL prototypes declared */
#include <GL/glew.h>
/* Using the GLUT library for the base windowing setup */
#include <GL/glut.h>
/* GLM */
// #define GLM_MESSAGES
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "shader_utils.h"

using namespace std;

class World;

class App {
  static World * world;
  static App * singleton;
public:
  int screen_width, screen_height;

  App(int argc, char *argv[]) {
    singleton = this;

    screen_width = 600;
    screen_height = 400;
    
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA|GLUT_ALPHA|GLUT_DOUBLE|GLUT_DEPTH);
    glutInitWindowSize(screen_width, screen_height);
    glutCreateWindow("Jumping Ball");

    GLenum glew_status = glewInit();
    if (glew_status != GLEW_OK) {
      fprintf(stderr, "Error: %s\n", glewGetErrorString(glew_status));
      return;
    }

    if (!GLEW_VERSION_2_0) {
      fprintf(stderr, "Error: your graphic card does not support OpenGL 2.0\n");
      return;
    }

    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);
  }

  App & setWorld(World & w);

  static void display();

  static void reshape(int width, int height) {
    singleton->screen_width = width;
    singleton->screen_height = height;
    glViewport(0, 0, width, height);
  }
  
  void run() {
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutMainLoop();
  }
};

#endif // APP_H

