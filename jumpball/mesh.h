#ifndef MESH_H
#define MESH_H

class Mesh {
private:
  GLuint vbo_vertices, vbo_normals, ibo_elements;
public:
  vector<glm::vec4> vertices;
  vector<glm::vec3> normals;
  vector<GLushort> elements;

  GLint attribute_v_coord, attribute_v_normal;
  
  Mesh() : vbo_vertices(0), vbo_normals(0), ibo_elements(0) {}
  ~Mesh() {
    if (vbo_vertices != 0)
      glDeleteBuffers(1, &vbo_vertices);
    if (vbo_normals != 0)
      glDeleteBuffers(1, &vbo_normals);
    if (ibo_elements != 0)
      glDeleteBuffers(1, &ibo_elements);
  }

  /**
   * Store object vertices, normals and/or elements in graphic card
   * buffers
   */
  void upload(App & app) {
    if (this->vertices.size() > 0) {
      glGenBuffers(1, &this->vbo_vertices);
      glBindBuffer(GL_ARRAY_BUFFER, this->vbo_vertices);
      glBufferData(GL_ARRAY_BUFFER,
                   this->vertices.size() * sizeof(this->vertices[0]),
                   this->vertices.data(), GL_STATIC_DRAW);
    }
    
    if (this->normals.size() > 0) {
      glGenBuffers(1, &this->vbo_normals);
      glBindBuffer(GL_ARRAY_BUFFER, this->vbo_normals);
      glBufferData(GL_ARRAY_BUFFER,
                   this->normals.size() * sizeof(this->normals[0]),
                   this->normals.data(), GL_STATIC_DRAW);
    }
    
    if (this->elements.size() > 0) {
      glGenBuffers(1, &this->ibo_elements);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->ibo_elements);
      glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                   this->elements.size() * sizeof(this->elements[0]),
                   this->elements.data(), GL_STATIC_DRAW);
    }
  }

  /**
   * Draw the object
   */
  void draw(App & app) {
    if (this->vbo_vertices != 0) {
      glEnableVertexAttribArray(attribute_v_coord);
      glBindBuffer(GL_ARRAY_BUFFER, this->vbo_vertices);
      glVertexAttribPointer(
        attribute_v_coord,  // attribute
        4,                  // number of elements per vertex, here (x,y,z,w)
        GL_FLOAT,           // the type of each element
        GL_FALSE,           // take our values as-is
        0,                  // no extra data between each position
        0                   // offset of first element
      );
    }

    if (this->vbo_normals != 0) {
      glEnableVertexAttribArray(attribute_v_normal);
      glBindBuffer(GL_ARRAY_BUFFER, this->vbo_normals);
      glVertexAttribPointer(
        attribute_v_normal, // attribute
        3,                  // number of elements per vertex, here (x,y,z)
        GL_FLOAT,           // the type of each element
        GL_FALSE,           // take our values as-is
        0,                  // no extra data between each position
        0                   // offset of first element
      );
    }
    
    /* Push each element in buffer_vertices to the vertex shader */
    if (this->ibo_elements != 0) {
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->ibo_elements);
      int size;  glGetBufferParameteriv(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);
      glDrawElements(GL_TRIANGLES, size/sizeof(GLushort), GL_UNSIGNED_SHORT, 0);
    } else {
      glDrawArrays(GL_TRIANGLES, 0, this->vertices.size());
    }

    if (this->vbo_normals != 0)
      glDisableVertexAttribArray(attribute_v_normal);
    if (this->vbo_vertices != 0)
      glDisableVertexAttribArray(attribute_v_coord);
  }
  
  void load_obj(const char* filename) {
    Mesh * mesh = this;
    ifstream in(filename, ios::in);
    if (!in) { cerr << "Cannot open " << filename << endl; exit(1); }
    vector<int> nb_seen;

    string line;
    while (getline(in, line)) {
      if (line.substr(0,2) == "v ") {
        istringstream s(line.substr(2));
        glm::vec4 v; s >> v.x; s >> v.y; s >> v.z; v.w = 1.0;
        mesh->vertices.push_back(v);
      }  else if (line.substr(0,2) == "f ") {
        istringstream s(line.substr(2));
        GLushort a,b,c;
        s >> a; s >> b; s >> c;
        a--; b--; c--;
        mesh->elements.push_back(a); mesh->elements.push_back(b); mesh->elements.push_back(c);
      }
      else if (line[0] == '#') { /* ignoring this line */ }
      else { /* ignoring this line */ }
    }

    mesh->normals.resize(mesh->vertices.size(), glm::vec3(0.0, 0.0, 0.0));
    nb_seen.resize(mesh->vertices.size(), 0);
    for (unsigned int i = 0; i < mesh->elements.size(); i+=3) {
      GLushort ia = mesh->elements[i];
      GLushort ib = mesh->elements[i+1];
      GLushort ic = mesh->elements[i+2];
      glm::vec3 normal = glm::normalize(glm::cross(
                                                   glm::vec3(mesh->vertices[ib]) - glm::vec3(mesh->vertices[ia]),
                                                   glm::vec3(mesh->vertices[ic]) - glm::vec3(mesh->vertices[ia])));

      int v[3];  v[0] = ia;  v[1] = ib;  v[2] = ic;
      for (int j = 0; j < 3; j++) {
        GLushort cur_v = v[j];
        nb_seen[cur_v]++;
        if (nb_seen[cur_v] == 1) {
          mesh->normals[cur_v] = normal;
        } else {
          // average
          mesh->normals[cur_v].x = mesh->normals[cur_v].x * (1.0 - 1.0/nb_seen[cur_v]) + normal.x * 1.0/nb_seen[cur_v];
          mesh->normals[cur_v].y = mesh->normals[cur_v].y * (1.0 - 1.0/nb_seen[cur_v]) + normal.y * 1.0/nb_seen[cur_v];
          mesh->normals[cur_v].z = mesh->normals[cur_v].z * (1.0 - 1.0/nb_seen[cur_v]) + normal.z * 1.0/nb_seen[cur_v];
          mesh->normals[cur_v] = glm::normalize(mesh->normals[cur_v]);
        }
      }
    }
  }

};


#endif // MESH_H
