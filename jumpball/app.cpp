#include "app.h"
#include "world.h"

App * App::singleton = NULL;

World * App::world = NULL;

App & App::setWorld(World & w) {
  world = &w;
  return *this;
}

void App::display() {
  if (world != NULL) {
    world->logic();
    glutPostRedisplay();
    world->draw();
  }
  glutSwapBuffers();
}
