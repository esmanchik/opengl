#include "app.h"
#include "world.h"

int main(int argc, char *argv[]) {
  try {
    string path = argc > 1 ? argv[1] : "ball.obj";
    Mesh ball;
    ball.load_obj(path.c_str());
    cout << "Ball has " << ball.vertices.size() << " vertices\n";

    App app(argc, argv);

    World w(app, "v.glsl", "f.glsl");
    w.setModel(ball);

    app.setWorld(w).run();

  } catch(exception &e) {
    cerr << e.what() << endl;
  }
  return 0;
}
