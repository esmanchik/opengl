#ifndef WORLD_H
#define WORLD_H

#include "app.h"
#include "mesh.h"

class World {
  App *app;
  GLuint program;
  Mesh *model;
  glm::mat4 object2world, world2camera, camera2screen;
  glm::vec2 screen;
  GLint uniform_m, uniform_v, uniform_p;
  GLint uniform_color;
  GLfloat velocity;
  int t0;

public:
  World(App &a, const char* vshader_filename, const char* fshader_filename) : object2world(glm::mat4(1.0)) {
    app = &a;

    float value = -4.5; // cin >> value;
    // View
    world2camera = /*glm::mat4(1.0f);*/
      glm::translate(
        glm::mat4(1.0f), glm::vec3(0.0, value / 3.0, value)
      );
    float angle = -30.0;
    glm::vec3 axis(0.0, 1.0, 0.0);
    world2camera = glm::rotate(
        world2camera, glm::radians(angle), axis
      );
    velocity = 10.0;
    t0 = glutGet(GLUT_ELAPSED_TIME);
    
    program = create_program(vshader_filename, fshader_filename);
    if (program == 0) 
      throw std::runtime_error("Failed to create program");

    uniform_m = get_uniform(program, "m");
    uniform_v = get_uniform(program, "v");
    uniform_p = get_uniform(program, "p");
    uniform_color = get_uniform(program, "shape_color");
  }
  
  ~World() { 
    if(program)
      glDeleteProgram(program); 
  }
  
  World & setModel(Mesh &m) {
    model = &m;
    model->attribute_v_coord = get_attrib(program, "v_coord");
    // model->attribute_v_normal = get_attrib(program, "v_normal");
    model->upload(*app);
    return *this;
  }

  void logic() {
    int t1 = glutGet(GLUT_ELAPSED_TIME);
    int dt = t1 - t0;
    float sec = dt / 1000.0;
    //if (sec < 2) cout << sec << " " << dt << endl;
    //cout << move << endl;
    //cin >> move;
    float g = 9.8;
    float v = velocity - g * sec;
    float height = v * sec;
    if (height < 0) {
      velocity = g * sec;
      height = 0.0;
      t0 = t1;
    }
    float angle = sec * 45;  // 45° per second
    float depth = -1 + 3 * sinf(t1 / 3000.0);
    glm::vec3 axis(0, 1, 0);
    glm::mat4 object2world = this->object2world;
    /*object2world = glm::rotate(
        object2world, glm::radians(angle), axis
      );*/
    object2world = glm::translate(
        object2world, glm::vec3(0.0, height, depth)
      );

    // View
    // glm::mat4 world2camera = glm::mat4(1.0f); //m_transform;

    // Projection
    camera2screen =
      glm::perspective(

        45.0f, 1.0f*app->screen_width/app->screen_height,
        0.1f, 100.0f
      );
    screen = glm::vec2(app->screen_width, app->screen_height);

    glUseProgram(program);

    /* Apply object's transformation matrix */
    glUniformMatrix4fv(uniform_m, 1, GL_FALSE, glm::value_ptr(object2world));
    glUniformMatrix4fv(uniform_v, 1, GL_FALSE, glm::value_ptr(world2camera));
    glUniformMatrix4fv(uniform_p, 1, GL_FALSE, glm::value_ptr(camera2screen));
    
    //glUniform2fv(uniform_p, 1, glm::value_ptr(screen));
  }

  void draw() {
    glClearColor(0.45, 0.45, 0.45, 1.0);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    glUseProgram(program);

    glm::vec3 color;

    color = glm::vec3(0.0, 1.0, 0.0);
    glUniform3fv(uniform_color, 1, glm::value_ptr(color));
    model->draw(*app);

    // glutWireCube(2.0);

    glm::mat4 object2world = glm::mat4(1.0f);
    glUniformMatrix4fv(uniform_m, 1, GL_FALSE, glm::value_ptr(object2world));

    color = glm::vec3(1.0, 0.0, 0.0);
    glUniform3fv(uniform_color, 1, glm::value_ptr(color));

    GLfloat vertices[] = {
      0.0, 0.0, 0.0,
      1.0, 0.0, 0.0,
      0.0, 0.0, 0.0,
      0.0, 1.0, 0.0,
      0.0, 0.0, 0.0,
      0.0, 0.0, 1.0
    };
    GLuint vbo_vertices;
    glGenBuffers(1, &vbo_vertices);
    glEnableVertexAttribArray(model->attribute_v_coord);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_vertices);
    glVertexAttribPointer(
        model->attribute_v_coord,  // attribute
        3,                  // number of elements per vertex, here (x,y,z,w)
        GL_FLOAT,           // the type of each element
        GL_FALSE,           // take our values as-is
        0,                  // no extra data between each position
        0                   // offset of first element
      );
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices),
                 vertices, GL_STATIC_DRAW);
    glDrawArrays(GL_LINES, 0, 6);
    glDeleteBuffers(1, &vbo_vertices);
  }
};

#endif // WORLD_H










