# README #

OpenGL techniques learned from [OpenGL Programming Book](https://en.wikibooks.org/wiki/OpenGL_Programming)

Runs even on [Raspberry PI 2](https://www.raspberrypi.org/blog/another-new-raspbian-release/) !
### How do I get set up? ###

Look [here](https://en.wikibooks.org/wiki/OpenGL_Programming/Installation/Linux) 
to install necessary libraries:
```
sudo apt-get install build-essential libgl1-mesa-dev
sudo apt-get install libglew-dev libglm-dev libfreetype6-dev
sudo apt-get install libsdl2-dev libsdl2-image-dev 
```
and install GLUT
```
sudo apt-get install freeglut3-dev
```